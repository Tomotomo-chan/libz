/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_fork.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mressier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/12 14:13:01 by mressier          #+#    #+#             */
/*   Updated: 2016/05/12 14:13:02 by mressier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libz_tools.h"
#include "ft_process.h"

int					ft_fork(void)
{
	int		pid;

	pid = fork();
	return (pid);
}

/*
** ft_fork_then
**
** fork and execute child_ft on the child and parent_ft on the parent
** - child_ft take a void *, which will be the content of child_param
** - parent_ft take an int, the pid of the child, and a void * wich will be
** the content of the parent_param
**
** it return the value returned by the parent function
*/
int				ft_fork_then(void (*child_ft)(void*),
								int (*parent_ft)(int, void*),
							void *child_param, void *parent_param)
{
	int		pid;

	pid = fork();
	if (pid == -1)
		return (-1);
	if (pid == FT_PROCESS_CHILD)
	{
		child_ft(child_param);
		exit(EXIT_SUCCESS);
	}
	return parent_ft(pid, parent_param);
}

void				ft_end_fork(void)
{
	return ;
}

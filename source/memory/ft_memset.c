/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memset.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mressier <mressier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/09 18:34:02 by mressier          #+#    #+#             */
/*   Updated: 2016/01/21 11:08:55 by mressier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**	     The memset() function writes mem_size bytes of value c (converted to an
**	     unsigned char) to the string mem.
*/

#include "libz_tools.h"

void		*ft_memset(void *mem, int c, size_t mem_size)
{
	unsigned char	*ret;

	ret = mem;
	while (mem_size--)
	{
		*ret = (unsigned char)c;
		ret++;
	}
	return (mem);
}

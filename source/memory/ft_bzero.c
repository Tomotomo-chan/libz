/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_bzero.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mressier <mressier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/09 18:43:17 by mressier          #+#    #+#             */
/*   Updated: 2016/01/21 11:03:55 by mressier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**	DESCRIPTION
**     The bzero() function writes mem_size zero bytes to mem.  If mem_size is
**     zero, bzero() does nothing.
*/

#include "libz_tools.h"

void	ft_bzero(void *mem, size_t mem_size)
{
	if (mem != NULL && mem_size != 0)
		ft_memset(mem, '\0', mem_size);
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_options_get_arguments.c                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mressier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/30 11:06:40 by mressier          #+#    #+#             */
/*   Updated: 2017/05/30 11:06:42 by mressier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_options.h"
#include "ft_str.h"

t_options_args		ft_options_get_arguments(int ac, char **av)
{
	int				i;

	if (ac == 1 || av == NULL)
		return (NULL);
	i = 1;
	while (i < ac)
	{
		if (av[i][0] != '-' || ft_strlen(av[i]) == 1)
			break ;
		if (ft_strlen(av[i]) == 2 && av[i][1] == '-')
		{
			i++;
			break ;
		}
		i++;
	}
	return (&av[i]);
}

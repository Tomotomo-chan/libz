/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_str_array_len.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mressier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/25 18:10:43 by mressier          #+#    #+#             */
/*   Updated: 2016/04/25 18:11:50 by mressier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libz_tools.h"

size_t			ft_str_tab_len(char **str)
{
	char		**p;

	if (str == NULL)
		return (0);
	p = str;
	while (*p != NULL)
		p++;
	return ((size_t)(p - str));
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libz_tools.h                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mressier <mressier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/09 18:37:43 by mressier          #+#    #+#             */
/*   Updated: 2017/11/12 11:30:12 by mressier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBZ_TOOLS_H
# define LIBZ_TOOLS_H

# include <sys/types.h>
# include <wchar.h>
# include <unistd.h>
# include <stdlib.h>
# include <stdarg.h>
# include <sys/ioctl.h>
# include <stdbool.h>
# include <limits.h>

/*
**  	------------------------------------------------------
**  	|						ALLOCATIONS         	     |
**  	------------------------------------------------------
*/

char				**ft_str_tab_new(size_t tab_size);
int					**ft_int_array_tab_new(size_t tab_size);

void				ft_intdel(int **int_tab);
void				ft_str_tab_del(char ***str_tab);
void				ft_int_array_tab_del(int ***int_tab, size_t int_tab_size);

/*
**  	------------------------------------------------------
**  	|						MEMORY	         	         |
**  	------------------------------------------------------
*/

void				*ft_memset(void *b, int c, size_t len);
void				ft_bzero(void *s, size_t n);
void				*ft_memcpy(void *dst, const void *src, size_t n);
void				*ft_memalloc(size_t size);
void				*ft_memmove(void *dst, const void *src, size_t len);
void				*ft_memccpy(void *dst, const void *src, int c, size_t n);
void				*ft_memchr(const void *s, int c, size_t n);
void				*ft_memrchr(const void *mem_start, int c, size_t n);
void				*ft_memdup(const void *s, size_t n);
void				ft_memdel(void **ap);
int					ft_memcmp(const void *s1, const void *s2, size_t n);
void				*ft_memjoin(const void *p1, size_t s1, const void *p2,
						size_t s2);

/*
**  	------------------------------------------------------
**  	|					  	  IS		       	         |
**  	------------------------------------------------------
*/

int					ft_isalnum(int c);
int					ft_isalpha(int c);
int					ft_isascii(int c);
int					ft_isdigit(int c);
int					ft_isprint(int c);
int					ft_isspace(int c);
int					ft_is_sort_descend(int *int_tab, size_t size);
int					ft_is_sort_ascend(int *int_tab, size_t size);
int					ft_is_lower(int n1, int n2);
int					ft_is_higher(int n1, int n2);
int					ft_str_is_digit(char *str);
int					ft_str_is_only_digit(char *str);

/*
**  	------------------------------------------------------
**  	|						ARRAY	         	         |
**  	------------------------------------------------------
*/

int					*ft_int_tab_new(size_t size);
void				ft_int_tab_rotate_down(int *int_tab, size_t size);
void				ft_int_tab_rotate_up(int *int_tab, size_t size);
void				ft_int_tab_go_up(int *int_tab, size_t size);
void				ft_int_tab_go_down(int *int_tab, size_t size);
bool				ft_int_tab_find_the_lower(int *int_tab, size_t size,
						int *lower);

size_t				ft_str_tab_len(char **str);

/*
**  	------------------------------------------------------
**  	|						DISPLAY	         	         |
**  	------------------------------------------------------
*/

/*
** ft_putchar.c
*/
void				ft_putchar(char c);
void				ft_putchar_fd(char c, int fd);

/*
** ft_putstr.c
*/
void				ft_putstr(const char *s);
void				ft_putstr_fd(const char *s, int fd);
void				ft_putstr_error(const char *str);

/*
** ft_putendl.c
*/
void				ft_putendl(const char *s);
void				ft_putendl_fd(const char *s, int fd);

/*
** ft_puterror.c
*/
void				ft_puterror(const char *s);

/*
** ft_putnstr.c
*/
void				ft_putnstr(const char *s, size_t n);
void				ft_putnstr_fd(const char *s, size_t n, int fd);

/*
** ft_putnbr.c
*/
void				ft_putnbr(int nb);
void				ft_putnbr_fd(int n, int fd);

/*
** ft_putnbrendl.c
*/
void				ft_putnbrendl(int nb);
void				ft_putnbrendl_fd(int nb, int fd);

/*
** ft_putnchar.c
*/
void				ft_putnchar(size_t n, int c);
void				ft_putnchar_fd(size_t n, int c, int fd);

/*
** ft_put_int_tab.c
*/
void				ft_put_int_tab(size_t nb_elem, int *int_tab);
void				ft_put_int_tab_fd(size_t nb_elem, int *int_tab, int fd);

/*
** ft_put_str_tab
*/
void				ft_put_str_tab(char **str_tab);
void				ft_put_str_tab_fd(char **str_tab, int fd);

/*
** ft_putwstr.c
*/
void				ft_putwstr(const wchar_t *ws);

/*
** ft_putnwstr.c
*/
void				ft_putnwstr(const wchar_t *ws, int size);
/*
** ft_putwchar.c
*/
int					ft_putwchar(wchar_t wc);

/*
** ft_put_color.c
*/
void				ft_putstr_color(const char *color, const char *s);
void				ft_putstr_color_fd(const char *color, const char *s,
						int fd);
void				ft_putnbr_color(const char *color, int n);

/*
** ft_print_hex.c
*/
void				ft_print_hex(size_t hex);
void				ft_print_hex_min(size_t hex);

/*
**  	------------------------------------------------------
**  	|						ARGUMENTS         	         |
**  	------------------------------------------------------
*/

void				*get_void_arg(va_list start, size_t nb_arg);
char				*get_str_arg(va_list start, size_t nb);
wchar_t				*get_wstr_arg(va_list start, size_t nb);
long long int		get_lli_arg(va_list start, size_t nb_arg);
long int			get_li_arg(va_list start, size_t nb);
int					get_int_arg(va_list start, size_t nb);
char				get_char_arg(va_list start, size_t nb);
wchar_t				get_wchar_arg(va_list start, size_t nb);
short				get_short_arg(va_list start, size_t nb);

/*
**  	------------------------------------------------------
**  	|						TERMINAL        	         |
**  	------------------------------------------------------
*/

size_t				get_terminal_nb_col(void);
size_t				get_terminal_nb_line(void);
int					*get_terminal_dimensions(void);
char				*create_new_path(char **path, const char *to_add);
char				*get_new_path(const char *path, const char *to_add);
char				*ft_get_host_name(void);
char				*ft_get_pwd(void);

#endif

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_hex.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mressier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/01 20:12:51 by mressier          #+#    #+#             */
/*   Updated: 2017/04/01 20:12:52 by mressier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libz_tools.h"

static void	internal_print_hex_with_ref(size_t hex, const char *ref)
{
	if (hex < 16)
		ft_putchar(ref[hex]);
	else
	{
		internal_print_hex_with_ref(hex / 16, ref);
		internal_print_hex_with_ref(hex % 16, ref);
	}
}

void		ft_print_hex(size_t hex)
{
	internal_print_hex_with_ref(hex, "0123456789ABCDEF");
}

void		ft_print_hex_min(size_t hex)
{
	internal_print_hex_with_ref(hex, "0123456789abcdef");
}

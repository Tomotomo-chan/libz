/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_rev_substr.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mressier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/27 16:16:36 by mressier          #+#    #+#             */
/*   Updated: 2017/01/17 19:07:41 by mressier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_str.h"
#include "libz_tools.h"

/*
** ft_rev_substr.c
**
** This function take a string, a start and a end and cut the part of the
** string between the start and the end
**
** @param	str				the string to cut
** @param	start			the index of the first character to cut
** @param	end				the index of the last character to cut
**
** @error str is NULL
** @error start is higher than end
** @error start is higher or equal to the len of str
**
** @return NULL on error, the new string otherwise
*/

char			*ft_rev_substr(const char *str, unsigned int start,
					unsigned int end)
{
	size_t				len;
	size_t				new_len;
	char				*new_str;

	if (str == NULL)
		return (NULL);
	len = ft_strlen(str);
	if (start > end || start >= len || end >= len)
		return (NULL);
	new_len = len - ((end - start) + 1);
	new_str = ft_strnew(new_len);
	if (new_str == NULL)
		return (NULL);
	ft_strncpy(new_str, str, start);
	ft_strcpy(&(new_str[start]), &(str[end + 1]));
	return (new_str);
}

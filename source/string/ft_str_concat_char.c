/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_str_concat_char.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mressier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/25 18:29:16 by mressier          #+#    #+#             */
/*   Updated: 2017/01/17 19:01:14 by mressier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_str.h"

char				*ft_str_concat_char(char *str, char c)
{
	int		i;

	if (str == NULL)
		return (NULL);
	i = ft_strlen(str);
	str[i] = c;
	return (str);
}

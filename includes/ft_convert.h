/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_convert.h                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tomo-chan <tomo-chan@student.42.fr>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/09 18:37:43 by mressier          #+#    #+#             */
/*   Updated: 2017/04/01 14:23:08 by mressier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_CONVERT_H
# define FT_CONVERT_H

# include <sys/types.h>

/*
** transform char
*/
int					ft_toupper(int c);
int					ft_tolower(int c);

/*
** number to string
*/
char				*ft_itoa(int n);
char				*ft_itoa_base(int nb, size_t base);
char				*ft_litoa_base(long int nb, size_t base);

char				ft_digit_to_char(int simple_digit);

/*
** string to number
*/
int					ft_atoi(const char *nptr);
long long int		ft_atolli(const char *nptr);

#endif

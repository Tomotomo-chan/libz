/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_process.h                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tomo-chan <tomo-chan@student.42.fr>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/09 18:37:43 by mressier          #+#    #+#             */
/*   Updated: 2017/04/01 14:23:13 by mressier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_PROCESS_H
# define FT_PROCESS_H

# define FT_PROCESS_CHILD		0

# define FT_PROCESS_PIPE_WRITE	1
# define FT_PROCESS_PIPE_READ	0

# include <fcntl.h>
# include <unistd.h>
# include <sys/wait.h>

int					ft_pipe(void);
int					ft_fork(void);
int					ft_fork_then(void (*child_ft)(void*),
								int (*parent_ft)(int, void*),
								void *child_param, void *parent_param);
void				ft_end_fork(void);

#endif

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_implode.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mressier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/25 18:08:36 by mressier          #+#    #+#             */
/*   Updated: 2016/04/25 18:08:38 by mressier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libz_tools.h"
#include "ft_str.h"

static int			internal_count_total_len(char **tab_str)
{
	int		i;
	int		len;

	i = 0;
	len = 0;
	while (tab_str[i])
	{
		len += ft_strlen(tab_str[i]);
		i++;
	}
	len += i;
	if (i > 0)
		len--;
	return (len);
}

/*
** ft_implode
** @note: same comportement as the php function implode()
**
** @param	tab_str			an array of str
** @param	c				the character use to concatene all the strings
**
** @error tab_str is NULL
** @error ft_strnew fail
**
** @return NULL on error, or an allocate string on success
*/

char				*ft_implode(char **tab_str, int c)
{
	int				i;
	int				len;
	char			*new_str;

	i = 0;
	if (tab_str == NULL)
		return (NULL);
	len = internal_count_total_len(tab_str);
	new_str = ft_strnew(len);
	if (new_str == NULL)
		return (NULL);
	while (tab_str[i])
	{
		new_str = ft_strcat(new_str, tab_str[i]);
		i++;
		if (tab_str[i])
			new_str = ft_str_concat_char(new_str, c);
	}
	return (new_str);
}

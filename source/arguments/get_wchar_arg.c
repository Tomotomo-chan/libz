/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_wchar_arg.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mressier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/28 14:32:37 by mressier          #+#    #+#             */
/*   Updated: 2016/03/31 17:16:32 by mressier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libz_tools.h"

wchar_t		get_wchar_arg(va_list start, size_t nb)
{
	return ((wchar_t)get_lli_arg(start, nb));
}

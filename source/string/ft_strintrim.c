/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtrim.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tomo-chan <tomo-chan@student.42.fr>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/24 15:58:31 by mressier          #+#    #+#             */
/*   Updated: 2016/05/17 12:07:01 by tomo-chan        ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_str.h"
#include "libz_tools.h"

#define INTERNAL_STRINTRIM_SPACE		' '

static char		internal_ft_replace_blank_with_space(char c)
{
	if (ft_isspace(c))
		return (INTERNAL_STRINTRIM_SPACE);
	return (c);
}

/*
** ft_strintrim
**
** Give a new allocated string with no blank at start and end
** and all spaces inside replace with a single space
*/

char			*ft_strintrim(const char *original_string)
{
	char	*trim_str;
	char	*temp_str;
	char	**split_str;

	if (original_string == NULL)
		return (NULL);
	temp_str = ft_strmap(original_string, internal_ft_replace_blank_with_space);
	split_str = ft_strsplit(temp_str, INTERNAL_STRINTRIM_SPACE);
	ft_strdel(&temp_str);
	temp_str = ft_implode(split_str, INTERNAL_STRINTRIM_SPACE);
	trim_str = ft_strtrim(temp_str);
	ft_str_tab_del(&split_str);
	ft_strdel(&temp_str);
	return (trim_str);
}

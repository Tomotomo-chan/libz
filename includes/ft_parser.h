/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_parser.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mressier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/12 12:37:44 by mressier          #+#    #+#             */
/*   Updated: 2017/11/12 12:37:48 by mressier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_PARSER_H
# define FT_PARSER_H

# include <unistd.h>

typedef struct		s_parser
{
	char			*name;
	char			*content;
	char			*cur_ptr;
}					t_parser;

int					ft_parser_init(const char *filename, t_parser *parser);
void				ft_parser_del(t_parser *parser);

/*
** ft_parser_line_and_char.c
*/
int					ft_parser_get_nb_line(const char *start, const char *ptr);
int					ft_parser_get_nb_char(const char *start, const char *ptr);
void				ft_parser_concat_line_and_char(const char *start,
						const char *end, char *str_error, size_t len);

#endif

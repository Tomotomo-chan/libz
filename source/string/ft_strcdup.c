/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcdup.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mressier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/26 19:03:37 by mressier          #+#    #+#             */
/*   Updated: 2016/04/26 19:03:38 by mressier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_str.h"

/*
**	Create a new string with str until the char c or until the end.
*/

char	*ft_strcdup(const char *str, int c)
{
	char	*new_str;
	int		len;

	len = 0;
	if (str == NULL)
		return (NULL);
	while (str[len])
	{
		if (str[len] == c)
			break ;
		len++;
	}
	new_str = ft_strsub(str, 0, len);
	return (new_str);
}

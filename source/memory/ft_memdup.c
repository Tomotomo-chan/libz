/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memdup.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mressier <mressier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/10 11:12:47 by mressier          #+#    #+#             */
/*   Updated: 2016/01/21 11:08:42 by mressier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**	Duplicate a zone of memory
*/

#include "libz_tools.h"

void	*ft_memdup(const void *mem, size_t mem_size)
{
	void			*mem_ret;

	if (mem == NULL || mem_size <= 0)
		return (NULL);
	mem_ret = ft_memalloc(mem_size);
	if (mem_ret == NULL)
		return (NULL);
	ft_memcpy(mem_ret, mem, mem_size);
	return (mem_ret);
}

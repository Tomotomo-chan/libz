/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_sqrt.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mressier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/01 11:20:37 by mressier          #+#    #+#             */
/*   Updated: 2017/06/01 11:20:38 by mressier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

size_t		ft_power(unsigned int n, unsigned int power)
{
	if (power == 0)
		return (1);
	return (n * ft_power(n, power - 1));
}

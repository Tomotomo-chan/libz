/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnbrendl.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mressier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/03 11:35:46 by mressier          #+#    #+#             */
/*   Updated: 2016/03/31 17:19:52 by mressier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**	Put a number with a \n
*/

#include "libz_tools.h"

void	ft_putnbrendl(int nb)
{
	ft_putnbrendl_fd(nb, 1);
}

void	ft_putnbrendl_fd(int nb, int fd)
{
	ft_putnbr_fd(nb, fd);
	ft_putchar_fd('\n', fd);
}

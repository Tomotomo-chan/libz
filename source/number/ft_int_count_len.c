/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_int_len.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mressier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/31 17:10:02 by mressier          #+#    #+#             */
/*   Updated: 2016/03/31 17:10:04 by mressier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_number.h"

/*
** get the number of digit composing a number
** it should be use to convert a number to a string
*/

int		ft_int_count_len(int nb)
{
	return (ft_signed_long_int_count_len(nb, 10));
}

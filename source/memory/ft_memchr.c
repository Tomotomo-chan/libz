/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memchr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mressier <mressier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/09 20:04:34 by mressier          #+#    #+#             */
/*   Updated: 2016/01/21 11:08:08 by mressier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**		DESCRIPTION
**	The memchr() function locates the first occurrence of c (converted to an
**	unsigned char) in string mem.
**		RETURN VALUES
**	The memchr() function returns a pointer to the byte located, or NULL if
**	no such byte exists within n bytes.
*/

#include "libz_tools.h"

void		*ft_memchr(const void *mem, int c, size_t n)
{
	unsigned char	*mem_ret;

	mem_ret = (unsigned char *)mem;
	while (n--)
	{
		if ((unsigned char)*mem_ret == (unsigned char)c)
			return (mem_ret);
		mem_ret++;
	}
	return (NULL);
}

/*
** ft_memchr
**
** Search the first character matching c from mem + n - 1 to mem
*/

void		*ft_memrchr(const void *mem_start, int c, size_t n)
{
	unsigned char	*mem_ret;

	mem_ret = (unsigned char *)mem_start + n - 1;
	while (n--)
	{
		if ((unsigned char)*mem_ret == (unsigned char)c)
			return (mem_ret);
		mem_ret--;
	}
	return (NULL);
}

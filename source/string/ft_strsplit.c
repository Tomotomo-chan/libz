/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsplit.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mressier <mressier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/10 14:25:50 by mressier          #+#    #+#             */
/*   Updated: 2016/01/21 11:14:36 by mressier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libz_tools.h"
#include "ft_str.h"

/*
**	Return an allocate array of string create by the split of the s with
** the char c
** s = "hello les moches" with the char ' ' make "hello" "les" "moches"
*/

static int		internal_count_part(const char *str, int c)
{
	int		part;
	char	*p_str;
	char	*p_next;

	part = 0;
	p_str = (char *)str;
	while (p_str && *p_str != '\0')
	{
		p_next = ft_strchr(p_str, c);
		if (p_next != p_str || p_next == str)
			part++;
		if (p_next == NULL || *p_next == '\0')
			break ;
		p_str = p_next + 1;
	}
	if (p_str && *p_str == '\0')
		part++;
	return (part);
}

char			**ft_strsplit(const char *str, char c)
{
	char			**tab;
	int				i_tab;
	int				nb_split;
	char			*p_str;

	if (str == NULL)
		return (NULL);
	nb_split = internal_count_part(str, c);
	tab = ft_str_tab_new(nb_split);
	p_str = (char *)str;
	i_tab = 0;
	while (i_tab < nb_split)
	{
		tab[i_tab] = ft_strcdup(p_str, c);
		i_tab++;
		p_str = ft_strchr_afer_next_occur(p_str, c);
	}
	return (tab);
}

char			**ft_strsplit_ntime(const char *str, char c, size_t max_split)
{
	char			**tab;
	int				i_tab;
	int				nb_split;
	char			*p_str;

	if (str == NULL)
		return (NULL);
	nb_split = internal_count_part(str, c);
	if (nb_split > (int)max_split + 1)
		nb_split = max_split + 1;
	tab = ft_str_tab_new(nb_split);
	p_str = (char *)str;
	i_tab = 0;
	while (i_tab < nb_split - 1)
	{
		tab[i_tab] = ft_strcdup(p_str, c);
		i_tab++;
		p_str = ft_strchr_afer_next_occur(p_str, c);
	}
	tab[i_tab] = ft_strdup(p_str);
	tab[i_tab + 1] = NULL;
	return (tab);
}

char			**ft_strsplit_once(const char *str, char c)
{
	return (ft_strsplit_ntime(str, c, 1));
}

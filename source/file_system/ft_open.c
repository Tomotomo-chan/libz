/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_open.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mressier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/30 15:21:32 by mressier          #+#    #+#             */
/*   Updated: 2017/05/30 15:21:34 by mressier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <fcntl.h>
#include <stdlib.h>
#include <errno.h>

int			ft_open(const char *filename, int flags, char **error_message)
{
	int		fd;

	fd = open(filename, flags);
	if (fd < 0 && error_message != NULL)
	{
		if (errno == EACCES)
			*error_message = "Permission denied";
		else if (errno == ENOENT)
			*error_message = "No such file or directory";
		else
			*error_message = "error";
	}
	return (fd);
}

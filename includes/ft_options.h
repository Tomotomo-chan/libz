/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_options.h                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mressier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/30 11:04:29 by mressier          #+#    #+#             */
/*   Updated: 2017/06/29 12:49:03 by mressier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_OPTIONS_H
# define FT_OPTIONS_H

# include <stdlib.h>
# include <stdbool.h>

# define CONTINUE_ON_ERROR	true
# define STOP_ON_ERROR		false

# define MAX_OPTIONS	128

typedef char **			t_options_args;
typedef	char			t_activ_options;

typedef struct			s_options
{
	t_options_args		args;
	unsigned int		nb_args;
	t_activ_options		options[MAX_OPTIONS];
}						t_options;

int						ft_get_options(int ac, char **av,
							const char *valid_options, t_options **options);

/*
**  	------------------------------------------------------
**  	|						STRUCTURE 	        	     |
**  	------------------------------------------------------
*/

t_options				*ft_t_options_new(void);
void					ft_t_options_del(t_options **opt);

/*
**  	------------------------------------------------------
**  	|					OPTIONS THEMSELVES        	     |
**  	------------------------------------------------------
*/

int						ft_options_get_options(int ac, char **av,
							const char *valid_options,
							t_activ_options *options);

/*
**  	------------------------------------------------------
**  	|						ARGUMENTS 	        	     |
**  	------------------------------------------------------
*/

t_options_args			ft_options_get_arguments(int ac, char **av);
int						ft_options_iter_arguments(t_options *opt,
							int continue_on_error,
							int (*ft_args)(const char *, t_options *));

#endif

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strstr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mressier <mressier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/10 12:10:09 by mressier          #+#    #+#             */
/*   Updated: 2016/01/21 11:14:43 by mressier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**	Catch the first occurence of the str little in the big, and return a pointer
**	on it, or NULL if it's not find on it
*/

#include "ft_str.h"

char		*ft_strstr(const char *haystack, const char *needle)
{
	size_t	len_needle;
	size_t	len_haystack;

	len_needle = ft_strlen(needle);
	len_haystack = ft_strlen(haystack);
	while (len_haystack >= len_needle)
	{
		if (ft_strncmp(haystack, needle, len_needle) == 0)
			return ((char *)haystack);
		haystack++;
		len_haystack--;
	}
	return (NULL);
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_str_tab_new.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mressier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/17 19:01:21 by mressier          #+#    #+#             */
/*   Updated: 2016/03/31 17:14:00 by mressier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libz_tools.h"

/*
** ft_str_tab_new
**
** This function create a table with tab_size
** @note	There is no '\0' for str_tab but we allocate + 1 for the last NULL
**
** @param	tab_size
**
** @error
**
** @return
*/

char		**ft_str_tab_new(size_t tab_size)
{
	char	**str;
	int		i;

	i = 0;
	str = ft_memalloc(sizeof(char *) * (tab_size + 1));
	if (str == NULL)
		return (NULL);
	while ((size_t)i < tab_size + 1)
	{
		str[i] = NULL;
		i++;
	}
	return (str);
}

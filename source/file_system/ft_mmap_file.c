/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_mmap_file.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mressier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/29 14:32:40 by mressier          #+#    #+#             */
/*   Updated: 2017/05/29 14:32:47 by mressier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <sys/stat.h>
#include <sys/mman.h>
#include <stdlib.h>
#include <stdio.h>

/*
** @error	in_fd is invalid
** @error	out_file_map or out_size is NULL
** @error	file is empty
** @error	mmap failed
*/

int				ft_mmap_file(int in_fd, void **out_file_mmap, size_t *out_size)
{
	struct stat		buf_stat;
	char			*file_mmap;

	if (in_fd < 0 || out_file_mmap == NULL || out_size == NULL)
		return (EXIT_FAILURE);
	if (fstat(in_fd, &buf_stat) != 0)
		return (EXIT_FAILURE);
	file_mmap = mmap(0, buf_stat.st_size, PROT_READ, MAP_PRIVATE, in_fd, 0);
	if (file_mmap == MAP_FAILED)
		return (EXIT_FAILURE);
	*out_size = buf_stat.st_size;
	*out_file_mmap = file_mmap;
	return (EXIT_SUCCESS);
}

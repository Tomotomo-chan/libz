/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_parser.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mressier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/12 12:35:54 by mressier          #+#    #+#             */
/*   Updated: 2017/11/12 12:36:47 by mressier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_system.h"
#include "ft_str.h"
#include "ft_parser.h"
#include <stdlib.h>

int			ft_parser_init(const char *filename, t_parser *parser)
{
	parser->name = (char *)filename;
	if (ft_read_file_with_filename(filename, &parser->content) == -1)
		return (EXIT_FAILURE);
	parser->cur_ptr = parser->content;
	return (EXIT_SUCCESS);
}

void		ft_parser_del(t_parser *parser)
{
	parser->name = NULL;
	ft_strdel(&(parser->content));
	parser->cur_ptr = NULL;
}

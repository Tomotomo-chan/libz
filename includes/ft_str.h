/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_str.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tomo-chan <tomo-chan@student.42.fr>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/09 18:37:43 by mressier          #+#    #+#             */
/*   Updated: 2017/01/17 19:00:18 by mressier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_STR_H
# define FT_STR_H

# include <unistd.h>
# include <wchar.h>

/*
**  	------------------------------------------------------
**  	|						ALLOCATIONS         	     |
**  	------------------------------------------------------
*/

char				*ft_strnew(size_t size);
char				*ft_strdup(const char *s1);
char				*ft_strndup(const char *s2, size_t n);
char				*ft_strcdup(const char *s1, int c);

/*
** strsplit
*/
char				**ft_strsplit(const char *s, char c);
char				**ft_strsplit_ntime(const char *s, char c, size_t n);
char				**ft_strsplit_once(const char *str, char c);

/*
** strjoin
*/
char				*ft_strjoin_and_replace(char **s1, const char *s2);
char				*ft_strjoin(const char *s1, const char *s2);
char				*ft_strnjoin(const char *s1, const char *s2, size_t n);

void				ft_strnjoin_and_replace(char **s1, const char *s2,
						size_t n);

/*
** strtrim
*/
char				*ft_strtrim(const char *s);
char				*ft_strintrim(const char *s);

char				*ft_strmap(const char *s, char (*f)(char));
char				*ft_strmapi(const char *s, char (*f)(unsigned int, char));

/*
** implode
*/
char				*ft_implode(char **tab_str, int c);

/*
** explode
*/
char				**ft_explode(const char *str, int c);

/*
** cut, sub
*/
char				*ft_rev_substr(const char *str, unsigned int start,
						unsigned int end);
char				*ft_strsub(const char *s, unsigned int start, size_t len);

/*
**  	------------------------------------------------------
**  	|						DESALLOCATIONS         	     |
**  	------------------------------------------------------
*/

void				ft_strclr(char *s);
void				ft_strdel(char **as);
void				ft_del_and_replace_str(char **str_to_replace,
						char *str_to_put);

/*
**  	------------------------------------------------------
**  	|						COPY-CONCAT	         	     |
**  	------------------------------------------------------
*/

/*
** strcpy
*/
char				*ft_strcpy(char *dst, const char *src);
char				*ft_strncpy(const char *dest, const char *src, size_t len);

/*
** strcat
*/
char				*ft_strcat(char *dest, const char *src);
char				*ft_str_concat_char(char *str, char c);
char				*ft_strcat_nb(char *str, int nb);
char				*ft_strncat_nb(char *str, int nb, size_t len);

char				*ft_strncat(char *dest, const char *src, size_t n);
size_t				ft_strlcat(char *dest, const char *src, size_t size);

/*
**  	------------------------------------------------------
**  	|					SEARCH, APPLY, ...         	     |
**  	------------------------------------------------------
*/

/*
** apply
*/
void				ft_striter(char *s, void (*f)(char *));
void				ft_striteri(char *s, void (*f)(unsigned int, char *));

/*
** search
*/
char				*ft_strrchr(const char *s, int c);
char				*ft_strchr(const char *s, int c);
char				*ft_strstr(const char *haystack, const char *needle);
char				*ft_strnstr(const char *haystack, const char *needle,
						size_t n);

char				*ft_strchr_afer_next_occur (const char *str, char c);

/*
** count char
*/
int					ft_count_char(const char *str, int c);
int					ft_count_char_with_limit(const char *str, const char *end,
						int c);

/*
** count_char_is
*/

int					ft_count_char_is(const char *str, int (*ft_is)(int));
int					ft_count_char_is_with_limit(const char *str,
						const char *end, int (*ft_is)(int));

/*
** ft_str_first.c
*/
char				*ft_str_first(const char *str, int (*ft_is)(int));
char				*ft_str_first_not(const char *str, int (*ft_is)(int));

/*
**  	------------------------------------------------------
**  	|							LEN		         	     |
**  	------------------------------------------------------
*/

/*
** len
*/
int					ft_strclen(const char *s, int c);
size_t				ft_strlen(const char *s);
size_t				ft_strnlen(const char *s, size_t maxlen);

/*
**  	------------------------------------------------------
**  	|						COMPARE 			   	     |
**  	------------------------------------------------------
*/

int					ft_strcmp(const char *s1, const char *s2);
int					ft_strncmp(const char *s1, const char *s2, size_t n);

int					ft_strequ(const char *s1, const char *s2);
int					ft_strequ_caseins(const char *s1, const char *s2);

int					ft_strnequ(const char *s1, const char *s2, size_t n);

/*
**  	------------------------------------------------------
**  	|						CONVERT 			   	     |
**  	------------------------------------------------------
*/

char				*ft_str_toupper(const char *str);
char				*ft_str_tolower(const char *str);

/*
**  	------------------------------------------------------
**  	|						OTHERS				   	     |
**  	------------------------------------------------------
*/

/*
**	Wchar & Wstr
*/
int					ft_wcharlen(wchar_t value);
wchar_t				*ft_wstrdup(const wchar_t *wstr);
size_t				ft_wstr_len(wchar_t	*wstr);

#endif

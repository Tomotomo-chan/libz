/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_count_char_is.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mressier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/12 14:27:47 by mressier          #+#    #+#             */
/*   Updated: 2017/11/12 14:27:48 by mressier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

int				ft_count_char_is(const char *str, int (*ft_is)(int))
{
	int		i;
	int		n;

	i = 0;
	n = 0;
	if (str == NULL)
		return (0);
	while (str[i])
	{
		if (ft_is(str[i]))
			n++;
		i++;
	}
	if (ft_is(str[i]))
		n++;
	return (n);
}

int				ft_count_char_is_with_limit(const char *str, const char *end,
					int (*ft_is)(int))
{
	int		n;

	n = 0;
	if (str == NULL)
		return (0);
	while (*str && str != end)
	{
		if (ft_is(*str))
			n++;
		str++;
	}
	if (ft_is(*str))
		n++;
	return (n);
}

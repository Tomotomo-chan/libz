/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libz.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tomo-chan <tomo-chan@student.42.fr>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/09 18:37:43 by mressier          #+#    #+#             */
/*   Updated: 2016/06/09 22:06:46 by tomo-chan        ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBZ_H
# define LIBZ_H

# include "libz_tools.h"
# include "get_next_line.h"
# include "color.h"
# include "ft_convert.h"
# include "ft_number.h"
# include "ft_process.h"
# include "ft_signal.h"
# include "ft_str.h"
# include "ft_system.h"
# include "ft_options.h"

#endif

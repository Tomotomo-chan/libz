/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strclr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mressier <mressier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/10 13:03:22 by mressier          #+#    #+#             */
/*   Updated: 2016/01/21 11:11:12 by mressier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**	Set all the char of the str at 0
*/

#include "ft_str.h"
#include "libz_tools.h"

void	ft_strclr(char *s)
{
	ft_bzero(s, ft_strlen(s));
}

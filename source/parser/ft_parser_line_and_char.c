/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_parser_get_line_and_char.c                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mressier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/12 13:29:43 by mressier          #+#    #+#             */
/*   Updated: 2017/11/12 13:29:44 by mressier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_convert.h"
#include "libz_tools.h"
#include "ft_str.h"

int				ft_parser_get_nb_line(const char *start, const char *ptr)
{
	if (start == ptr)
		return (0);
	return (ft_count_char_with_limit(start, ptr - 1, '\n'));
}

int				ft_parser_get_nb_char(const char *start, const char *ptr)
{
	char	*prev_newline;
	int		size;

	size = 0;
	if (start >= ptr)
		return (0);
	prev_newline = ft_memrchr(start, '\n', ptr - start);
	if (prev_newline == NULL)
		size += (ptr - start) + 1;
	else
		size += ptr - prev_newline;
	return (size);
}

void			ft_parser_concat_line_and_char(const char *start,
					const char *ptr, char *str_error, size_t len)
{
	int		nb_line;
	int		nb_char;

	if (!start || !ptr || !str_error)
	{
		ft_strncat(str_error, "\"[null]\"", len);
		return ;
	}
	nb_line = ft_parser_get_nb_line(start, ptr);
	nb_char = ft_parser_get_nb_char(start, ptr);
	ft_strncat(str_error, "[", len);
	ft_strncat_nb(str_error, nb_line + 1, len);
	ft_strncat(str_error, ",", len);
	ft_strncat_nb(str_error, nb_char, len);
	ft_strncat(str_error, "]", len);
}

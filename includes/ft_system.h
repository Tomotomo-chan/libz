/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_system.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mressier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/23 19:25:08 by mressier          #+#    #+#             */
/*   Updated: 2017/11/11 16:06:46 by mressier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_SYSTEM_H
# define FT_SYSTEM_H

# include <grp.h>
# include <sys/stat.h>

enum					e_file_mode
{
	FIFO = 0,
	SPE_CHAR,
	SPE_BLK,
	DIRECTORY,
	SYM_LINK,
	SOCK_LINK,
	REG_FILE,
	UNKNOW
};

enum e_file_mode		ft_get_file_mode(mode_t file_mode);

int						ft_open(const char *filename, int flags,
							char **error_message);

int						ft_mmap_file(int in_fd, void **out_file_mmap,
							size_t *out_size);
int						ft_munmap_file(void *in_file_mmap, size_t size);

int						ft_read_file(int fd, char **out_content);
int						ft_read_file_with_filename(const char *filename,
							char **out_content);

#endif

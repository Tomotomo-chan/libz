/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_un_len.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mressier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/28 13:50:49 by mressier          #+#    #+#             */
/*   Updated: 2016/02/03 11:06:18 by mressier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_number.h"

/*
** Return the len of the number in base <base> converted to string.
** If the <base> is lower than 1 or higher than 16, 0 is returned.
*/

int		ft_unsigned_long_int_count_len(unsigned long long int nb, size_t base)
{
	if (base < 2 || base > 16)
		return (0);
	if (nb < base)
		return (1);
	else
		return (1 + ft_unsigned_long_int_count_len(nb / base, base));
}

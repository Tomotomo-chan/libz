/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_get_options.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mressier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/30 13:59:40 by mressier          #+#    #+#             */
/*   Updated: 2017/05/30 13:59:41 by mressier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libz_tools.h"
#include "ft_options.h"

int			ft_get_options(int ac, char **av, const char *valid_options,
				t_options **options)
{
	t_options		*opt;
	int				ret;

	*options = NULL;
	opt = ft_t_options_new();
	if (opt == NULL)
	{
		ft_putstr_error("ft_t_options_new() fail\n");
		return (EXIT_FAILURE);
	}
	ret = ft_options_get_options(ac, av, valid_options, opt->options);
	if (ret != EXIT_SUCCESS)
	{
		ft_t_options_del(&opt);
		return (ret);
	}
	opt->args = ft_options_get_arguments(ac, av);
	opt->nb_args = ft_str_tab_len(opt->args);
	*options = opt;
	return (EXIT_SUCCESS);
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strequ.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mressier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/24 15:54:47 by mressier          #+#    #+#             */
/*   Updated: 2017/01/17 18:59:58 by mressier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**	Return 1 or 0 if the str are equal or not
** if both s1 and s2 are NULL, they are like equal
** if only one of them is NULL, there are not equals
*/

#include "ft_str.h"
#include <stdbool.h>

int			ft_strequ(const char *s1, const char *s2)
{
	if (s1 == NULL && s2 == NULL)
		return (true);
	if (s1 == NULL || s2 == NULL)
		return (false);
	if (ft_strcmp(s1, s2) == 0)
		return (true);
	return (false);
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_intdel.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mressier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/28 15:52:14 by mressier          #+#    #+#             */
/*   Updated: 2016/03/31 17:13:45 by mressier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libz_tools.h"

/*
** ft_intdel
**
** This function free and make pointer to NULL for int *
**
** @param	tab			the pointer on the int *
**
** @error
**
** @return			void
*/

void	ft_intdel(int **tab)
{
	ft_memdel((void **)tab);
}

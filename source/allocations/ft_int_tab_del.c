/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_int_tab_del.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mressier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/28 15:49:42 by mressier          #+#    #+#             */
/*   Updated: 2016/03/31 17:13:34 by mressier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libz_tools.h"

/*
** ft_int_array_tab_del
**
**	This function free all the int * inside the tab and then free the tab itself
**
** @param	tab
** @param	tab_size
**
** @error
**
** @return	void
*/

void				ft_int_array_tab_del(int ***tab, size_t tab_size)
{
	size_t	indx_col;

	indx_col = 0;
	if (tab != NULL && *tab != NULL)
	{
		while (indx_col < tab_size)
		{
			ft_intdel(&((*tab)[indx_col]));
			indx_col++;
		}
		ft_memdel((void **)tab);
	}
}

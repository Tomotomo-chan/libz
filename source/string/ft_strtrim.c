/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtrim.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tomo-chan <tomo-chan@student.42.fr>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/24 15:58:31 by mressier          #+#    #+#             */
/*   Updated: 2016/05/17 12:07:01 by tomo-chan        ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
** return a copy of s without blank at start and end of the str
*/

#include "libz_tools.h"
#include "ft_str.h"

char			*ft_strtrim(const char *s)
{
	size_t				i;
	size_t				start;

	i = 0;
	if (!s)
		return (NULL);
	while (ft_isspace(s[i]))
		i++;
	if ((start = i) == ft_strlen(s))
		return (ft_strdup(""));
	i = ft_strlen(s) - 1;
	while (ft_isspace(s[i]))
		i--;
	return (ft_strsub(s, start, i - start + 1));
}

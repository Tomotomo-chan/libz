/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcat_nb.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mressier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/12 13:44:06 by mressier          #+#    #+#             */
/*   Updated: 2017/11/12 13:44:32 by mressier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_str.h"
#include "ft_convert.h"
#include <stdlib.h>

char		*ft_strcat_nb(char *str, int nb)
{
	char	*temp;

	temp = ft_itoa(nb);
	str = ft_strcat(str, temp);
	free(temp);
	return (str);
}

char		*ft_strncat_nb(char *str, int nb, size_t len)
{
	char	*temp;

	temp = ft_itoa(nb);
	str = ft_strncat(str, temp, len);
	free(temp);
	return (str);
}

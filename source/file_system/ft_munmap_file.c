/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_munmap_file.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mressier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/30 10:15:32 by mressier          #+#    #+#             */
/*   Updated: 2017/05/30 10:15:33 by mressier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <sys/mman.h>
#include <stdlib.h>

int			ft_munmap_file(void *in_file_mmap, size_t size)
{
	if (in_file_mmap == NULL)
		return (EXIT_FAILURE);
	if (munmap(in_file_mmap, size) != 0)
		return (EXIT_FAILURE);
	return (EXIT_SUCCESS);
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_options_iter_arguments.c                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mressier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/30 11:23:50 by mressier          #+#    #+#             */
/*   Updated: 2017/05/30 11:23:52 by mressier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_options.h"

int		ft_options_iter_arguments(t_options *opt, int continue_on_error,
			int (*ft_args)(const char *, t_options *))
{
	int		i;
	int		ret;

	i = 0;
	ret = EXIT_SUCCESS;
	if (opt == NULL || opt->args == NULL || ft_args == NULL)
		return (EXIT_FAILURE);
	while (opt->args[i])
	{
		if (ft_args(opt->args[i], opt) == EXIT_FAILURE)
		{
			if (continue_on_error == true)
				ret = EXIT_FAILURE;
			else
				return (EXIT_FAILURE);
		}
		i++;
	}
	return (ret);
}

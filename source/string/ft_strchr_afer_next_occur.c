/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strchr_afer_next_occur.c                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mressier <mressier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/10 14:25:50 by mressier          #+#    #+#             */
/*   Updated: 2016/01/21 11:14:36 by mressier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

/*
** This function found the first character after the next occurence of c
** str = 'hello world' and c = ' ' return a pointer on 'world'
*/

char			*ft_strchr_afer_next_occur(const char *str, char c)
{
	char		*p_str;

	if (str == NULL)
		return (NULL);
	p_str = (char *)str;
	while (*p_str && *p_str != c)
		p_str++;
	if (*p_str == '\0')
		return (NULL);
	while (*p_str && *p_str == c)
		p_str++;
	return (p_str);
}

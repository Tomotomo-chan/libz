/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnstr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mressier <mressier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/09 19:19:06 by mressier          #+#    #+#             */
/*   Updated: 2016/03/31 17:20:01 by mressier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
** Put at most n character
*/

#include "ft_str.h"

void	ft_putnstr_fd(const char *s, size_t n, int fd)
{
	size_t		len;

	len = ft_strlen(s);
	if (n < len)
		write(fd, s, n);
	else
		write(fd, s, len);
}

void	ft_putnstr(const char *s, size_t n)
{
	ft_putnstr_fd(s, n, 1);
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   make_a_pipe.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mressier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/18 10:35:23 by mressier          #+#    #+#             */
/*   Updated: 2016/05/18 10:35:25 by mressier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_process.h"

int				ft_pipe(void)
{
	int		pipe_tab[2];
	int		pid;

	if (pipe(pipe_tab) == -1)
	{
		return (-1);
	}
	pid = ft_fork();
	if (pid == FT_PROCESS_CHILD)
	{
		dup2(pipe_tab[FT_PROCESS_PIPE_WRITE], STDOUT_FILENO);
		close(pipe_tab[FT_PROCESS_PIPE_READ]);
	}
	dup2(pipe_tab[FT_PROCESS_PIPE_READ], STDIN_FILENO);
	close(pipe_tab[FT_PROCESS_PIPE_WRITE]);
	return (pid);
}

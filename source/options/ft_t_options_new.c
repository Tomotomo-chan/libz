/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_t_options_new.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mressier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/30 13:14:26 by mressier          #+#    #+#             */
/*   Updated: 2017/05/30 13:14:27 by mressier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdbool.h>
#include "ft_options.h"
#include "libz_tools.h"

static void				internal_init_activ_options(t_activ_options *opts)
{
	int		i;

	i = 0;
	while (i < MAX_OPTIONS)
	{
		opts[i] = false;
		i++;
	}
}

t_options				*ft_t_options_new(void)
{
	t_options	*opt;

	opt = (t_options *)ft_memalloc(sizeof(t_options));
	if (opt == NULL)
		return (NULL);
	opt->args = NULL;
	internal_init_activ_options(opt->options);
	return (opt);
}

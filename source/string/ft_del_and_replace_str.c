/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_str_replace_str.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mressier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/31 17:10:15 by mressier          #+#    #+#             */
/*   Updated: 2017/01/17 19:07:53 by mressier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_str.h"

/*
** ft_str_replace_str
**
** free the str_to_replace and set it to str_to_put
*/

void		ft_del_and_replace_str(char **str_to_replace,
									char *str_to_put)
{
	if (str_to_replace != NULL)
	{
		ft_strdel(str_to_replace);
		*str_to_replace = str_to_put;
	}
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_int_tab_new.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mressier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/17 18:56:29 by mressier          #+#    #+#             */
/*   Updated: 2016/03/31 17:13:42 by mressier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libz_tools.h"

/*
** ft_int_array_tab_new
**
** This function create an array of tab_size.
**
** @param	tab_size			the number of line wanted in the table
**
** @error	ft_memalloc() fail
**
** @return
*/

int			**ft_int_array_tab_new(size_t tab_size)
{
	int		**tab;
	int		i;

	i = 0;
	if (tab_size == 0)
		return (NULL);
	tab = ft_memalloc(sizeof(int *) * tab_size);
	if (tab == NULL)
		return (NULL);
	while ((size_t)i < tab_size)
	{
		tab[i] = NULL;
		i++;
	}
	return (tab);
}

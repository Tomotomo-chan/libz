/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_signed_len.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mressier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/28 18:23:37 by mressier          #+#    #+#             */
/*   Updated: 2016/02/03 11:05:56 by mressier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_number.h"

/*
** Return the number of digits which composed the number, convert in base
** <base>. If the <base> is lower than 2 or higher than 16, it returns 0.
*/

int		ft_signed_long_int_count_len(long long int nb, size_t base)
{
	if (base < 2 || base > 16)
		return (0);
	if (nb < 0)
	{
		if (base == 10)
			return (1 + ft_unsigned_long_int_count_len(-nb, base));
		else
			return (ft_unsigned_long_int_count_len(-nb, base));
	}
	if (nb < (long long int)base)
		return (1);
	else
		return (1 + ft_signed_long_int_count_len(nb / base, base));
}

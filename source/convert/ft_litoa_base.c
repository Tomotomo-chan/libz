/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_llitoa_base.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mathou <mathou@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/28 17:54:16 by mressier          #+#    #+#             */
/*   Updated: 2016/03/31 17:17:41 by mressier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_convert.h"
#include "ft_number.h"
#include "ft_str.h"

char	*ft_litoa_base(long int nb, size_t base)
{
	char			*str;
	int				size;
	size_t			big_nb;

	size = ft_signed_long_int_count_len(nb, base);
	str = ft_strnew(size);
	if (str == NULL)
		return (NULL);
	if (nb < 0)
		str[0] = '-';
	big_nb = nb < 0 ? (size_t)nb * -1 : (size_t)nb;
	while (big_nb > base - 1)
	{
		str[--size] = ft_digit_to_char(big_nb % base);
		big_nb /= base;
	}
	str[--size] = ft_digit_to_char(big_nb);
	return (str);
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_reset_signal.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mressier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/21 21:09:09 by mressier          #+#    #+#             */
/*   Updated: 2016/04/21 21:09:11 by mressier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_signal.h"

void			ft_reset_multi_signal(int *sig, int nb)
{
	int		i;

	i = 0;
	if (sig != NULL)
	{
		while (i < nb)
		{
			ft_reset_signal(sig[i]);
			i++;
		}
	}
}

void			ft_reset_signal(int sig)
{
	ft_init_signal(sig, SIG_DFL);
}

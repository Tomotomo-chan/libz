/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_is_sort_ascend.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mressier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/07 10:35:22 by mressier          #+#    #+#             */
/*   Updated: 2016/03/31 17:20:50 by mressier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <stdbool.h>

/*
** ft_is_sort_ascend
**
** check if a int table is sort by ascent order
**
** @param	tab
** @param	size
**
** @error	tab is NULL
**
** @return true if tab is sort ascend, false if is not or if tab is NULL
*/

int		ft_is_sort_ascend(int *tab, size_t size)
{
	size_t	i;

	i = 0;
	if (tab == NULL || size == 0)
		return (false);
	while (i < size - 1)
	{
		if (tab[i + 1] < tab[i])
			return (false);
		i++;
	}
	return (true);
}

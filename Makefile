# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: mathou <mathou@student.42.fr>              +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2016/01/10 10:00:41 by mressier          #+#    #+#              #
#    Updated: 2016/02/22 11:32:14 by mressier         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = libZ.a

C_DIR =	source/
C_DIRS = $(shell find $(C_DIR) -type d -follow -print)
C_FILES = $(shell find $(C_DIR) -type f -follow -print | grep "\.c")

O_DIR =	.tmp/obj
O_DIRS = $(C_DIRS:$(C_DIR)%=$(O_DIR)%)
O_FILES = $(C_FILES:$(C_DIR)%.c=$(O_DIR)%.o)

FLAGS = -Wall -Wextra -Werror -fPIC
INCLUDES = -I ./includes

all: $(NAME)

$(NAME): $(O_FILES)
	@ar rcs $@ $^
	@ranlib $@
	@printf "\r[\033[32m CREATED \033[0m]\t\033[33m$(NAME)\033[0m\n"

$(O_DIR)%.o: $(C_DIR)%.c
	@tput civis
	@mkdir -p $(O_DIRS) $(O_DIR)
	@printf "[\033[32m $(NAME) \033[0m]\t\033[33m$<\033[0m\\n" | sed 's@$(C_DIR)/@@'
	@gcc $(INCLUDES) $(FLAGS) -o $@ -c $<
	@tput cnorm

clean:
	@echo "[ \033[32mCLEAN \033[0m]\t\033[33mRemove all objets for $(NAME)\033[0m"
	@rm -rf $(O_DIR)

fclean: clean
	@echo "[ \033[32mCLEAN \033[0m]\t\033[33mRemove all binaries for $(NAME)\033[0m"
	@rm -rf $(NAME)
	@rm -rf .tmp/

re: fclean all

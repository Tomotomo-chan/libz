/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnfjoin.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mressier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/26 12:59:29 by mressier          #+#    #+#             */
/*   Updated: 2016/02/26 12:59:30 by mressier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_str.h"

/*
** create an allocate string concatenating s1 and the n first char of s2
** free s1 and set the new string instead
*/

void			ft_strnjoin_and_replace(char **s1, const char *s2, size_t n)
{
	char	*new_str;

	if (s1 == NULL || *s1 == NULL)
		return ;
	if (s2 == NULL)
		new_str = NULL;
	else
		new_str = ft_strnjoin(*s1, s2, n);
	ft_strdel(s1);
	*s1 = new_str;
}

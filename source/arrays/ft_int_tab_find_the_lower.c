/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_find_the_lower.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mressier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/11 20:08:47 by mressier          #+#    #+#             */
/*   Updated: 2016/03/31 17:16:40 by mressier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libz_tools.h"

/*
** ft_int_tab_find_the_lower
**
** What does this function ? //TEMP
**
** @param	tab
** @param	tab_size
** @param	lower
**
** @error	tab is NULL, lower is NULL
** @error	tab_size is 0
**
** @return true if we find the lower, false if an error occur
*/

bool		ft_int_tab_find_the_lower(int *tab, size_t tab_size, int *lower)
{
	int			tmp_lower;
	size_t		index;

	if (tab == NULL || lower == NULL || tab_size == 0)
		return (false);
	index = 0;
	tmp_lower = tab[0];
	while (index < tab_size)
	{
		if (tab[index] < tmp_lower)
			tmp_lower = tab[index];
		index++;
	}
	*lower = tmp_lower;
	return (true);
}

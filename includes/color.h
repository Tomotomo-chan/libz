/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   color.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mressier <mressier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/12 11:22:35 by mressier          #+#    #+#             */
/*   Updated: 2017/04/01 14:23:05 by mressier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef COLOR_H
# define COLOR_H

# define C_CLEAR 	"\033[0m"
# define C_YELLOW 	"\033[33m"
# define C_RED 		"\033[31m"
# define C_GREEN 	"\033[32m"
# define C_CYAN 	"\033[36m"
# define C_WHITE 	"\033[37m"
# define C_BLUE 	"\033[34m"
# define C_PURPLE 	"\033[35m"
# define C_BLACK    "\033[1;30m"
# define C_G_YELLOW "\033[33;1m"
# define C_G_RED 	"\033[31;1m"
# define C_G_GREEN 	"\033[32;1m"
# define C_G_CYAN 	"\033[36;1m"
# define C_G_WHITE 	"\033[37;1m"
# define C_G_BLUE 	"\033[34;1m"
# define C_G_PURPLE "\033[35;1m"

# define BC_YELLOW 	"\033[43;1m"
# define BC_GREEN	"\033[42;1m"
# define BC_RED		"\033[41;1m"
# define BC_CYAN 	"\033[46;1m"
# define BC_BLUE 	"\033[44;1m"
# define BC_PURPLE 	"\033[45;1m"

#endif

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_number.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mressier <mressier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/09 18:37:43 by mressier          #+#    #+#             */
/*   Updated: 2017/09/29 12:19:08 by mressier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_NUMBER_H
# define FT_NUMBER_H

# include <sys/types.h>
# include <stdint.h>

/*
**  	------------------------------------------------------
**  	|						CONVERT		        	     |
**  	------------------------------------------------------
*/

void				ft_swap(int *a, int *b);
uint16_t			ft_swap_uint16(uint16_t n);
uint32_t			ft_swap_uint32(uint32_t n);
uint64_t			ft_swap_uint64(uint64_t n);

int					ft_unsigned_long_int_count_len(unsigned long long int nb,
						size_t base);
int					ft_signed_long_int_count_len(long long int nb, size_t base);
int					ft_int_count_len(int nb);

/*
**  	------------------------------------------------------
**  	|						CALCUL 		        	     |
**  	------------------------------------------------------
*/

size_t				ft_power(unsigned int n, unsigned int power);
int					ft_abs(int value);

#endif

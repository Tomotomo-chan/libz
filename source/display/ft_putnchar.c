/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnchar.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mressier <mressier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/09 19:19:19 by mressier          #+#    #+#             */
/*   Updated: 2016/03/31 17:19:56 by mressier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**	Display the char c n times
*/

#include "libz_tools.h"

#define STR_SIZE	20

void	ft_putnchar(size_t n, int c)
{
	ft_putnchar_fd(n, c, 1);
}

void	ft_putnchar_fd(size_t n, int c, int fd)
{
	char		temp_string[STR_SIZE + 1];

	ft_memset(temp_string, c, STR_SIZE);
	temp_string[STR_SIZE] = '\0';
	while (n >= STR_SIZE)
	{
		ft_putstr_fd(temp_string, fd);
		n -= STR_SIZE;
	}
	temp_string[n] = '\0';
	ft_putstr_fd(temp_string, fd);
}

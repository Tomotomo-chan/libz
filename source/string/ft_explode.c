/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_explode.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tomo-chan <tomo-chan@student.42.fr>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/02 10:20:37 by mressier          #+#    #+#             */
/*   Updated: 2017/01/17 19:01:50 by mressier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**	This function is pretty similar to strsplit, except that it split on all
**	the c char.
**	Example : "toto==truc" on '=' give
**	[0] = toto, [1] = "", [2] = "truc"
*/

#include "libz_tools.h"
#include "ft_str.h"

char			**ft_explode(const char *str, int c)
{
	char	**tab;
	int		tab_size;
	int		i;
	int		j;

	i = 0;
	j = 0;
	if (str == NULL)
		return (NULL);
	tab_size = ft_count_char(str, c) + 1;
	tab = ft_str_tab_new(tab_size);
	while (j < tab_size)
	{
		tab[j] = ft_strcdup(&str[i], c);
		i += ft_strlen(tab[j]);
		j++;
		if (str[i] == c)
			i++;
	}
	return (tab);
}

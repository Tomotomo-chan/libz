/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_signal.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mressier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/21 20:57:22 by mressier          #+#    #+#             */
/*   Updated: 2017/04/01 14:23:15 by mressier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_SIGNAL_H
# define FT_SIGNAL_H

# include <signal.h>
# include <string.h>

void			ft_init_multi_signal(int *sig, int nb, void (*handler)(int));
void			ft_reset_multi_signal(int *sig, int nb);
void			ft_init_signal(int sig, void (*handler)(int));
void			ft_reset_signal(int sig);

#endif

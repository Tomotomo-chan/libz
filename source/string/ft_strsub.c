/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsub.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mressier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/24 15:58:22 by mressier          #+#    #+#             */
/*   Updated: 2016/01/21 11:15:26 by mressier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_str.h"

/*
** Return an allocate string of the piece of s (since s[start]
** with a lenght of len)
*/

char		*ft_strsub(const char *s, unsigned int start, size_t len)
{
	char	*new_s;

	if (s == NULL || start > ft_strlen(s))
		return (NULL);
	new_s = ft_strndup(&(s[start]), len);
	return (new_s);
}

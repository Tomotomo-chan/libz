/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_options_get_options.c                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mressier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/07 11:58:17 by mressier          #+#    #+#             */
/*   Updated: 2017/06/07 11:58:18 by mressier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_options.h"
#include "libz_tools.h"
#include "ft_str.h"

static int		internal_set_activ_options(const char *str_opt,
					const char *valid_options, t_activ_options *options)
{
	unsigned int		i;

	i = 1;
	while (str_opt[i] != '\0')
	{
		if (ft_isalnum(str_opt[i]) == false
				|| ft_strchr(valid_options, str_opt[i]) == NULL)
			return (str_opt[i]);
		options[(int)str_opt[i]] = true;
		i++;
	}
	return (EXIT_SUCCESS);
}

/*
** ft_options_get_options
**
** @error: return the bad options character
*/

int				ft_options_get_options(int ac, char **av,
					const char *valid_options, t_activ_options *options)
{
	int			i;
	int			ret;

	if (ac < 1 || av == NULL || options == NULL)
		return (EXIT_FAILURE);
	i = 1;
	while (i < ac)
	{
		if (av[i][0] != '-' || ft_strlen(av[i]) == 1)
			break ;
		if (ft_strlen(av[i]) == 2 && av[i][1] == '-')
			break ;
		ret = internal_set_activ_options(av[i], valid_options, options);
		if (ret != EXIT_SUCCESS)
			return (ret);
		i++;
	}
	return (EXIT_SUCCESS);
}

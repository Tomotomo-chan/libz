/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_go_down.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mressier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/06 17:34:01 by mressier          #+#    #+#             */
/*   Updated: 2016/03/31 17:16:47 by mressier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libz_tools.h"

/*
** ft_int_tab_go_down
**
** If init tab is like { 10, 20, 5, 3} it go down => { 0, 10, 20, 5 }
** tab[0] is set to 0
**
** @param	tab
** @param	size
**
** @return none
*/

void	ft_int_tab_go_down(int *tab, size_t size)
{
	int		i;

	if (tab != NULL && size != 0)
	{
		i = size - 1;
		while (i)
		{
			tab[i] = tab[i - 1];
			i--;
		}
		tab[i] = 0;
	}
}

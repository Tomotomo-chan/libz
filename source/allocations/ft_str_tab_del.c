/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_str_tab_del.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mressier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/28 15:49:49 by mressier          #+#    #+#             */
/*   Updated: 2016/03/31 17:13:51 by mressier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libz_tools.h"

/*
** ft_str_tab_del
**
** This function free all the str contain in the table and then the table itself
**
** @param	str_tab			The table of strings
**
** @error
**
** @return
*/

void		ft_str_tab_del(char ***str_tab)
{
	int		index;

	index = 0;
	if (str_tab != NULL && *str_tab != NULL)
	{
		while (*str_tab && (*str_tab)[index])
		{
			ft_memdel((void **)&((*str_tab)[index]));
			index++;
		}
		ft_memdel((void **)str_tab);
	}
}
